<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Tests\Datatourisme\Api\Processor\Filter;

use Tests\Datatourisme\Api\Processor\AbstractGraphQLTest;

class SpecialTest extends AbstractGraphQLTest
{
    public function testNowKeyword()
    {
        $res = $this->queryGraqhQL('
        {
            poi(
                filters: [
                  {takesPlaceAt: {
                     startDate: {
                        _lt: "_now_"
                     }
                  }}
               ]
            )
            {
                total,
                results {
                    dc_identifier
                }
            }
        }');
        $res = json_decode($res, true);
        $this->assertTrue($res['data']['poi']['total'] > 0);
    }
}
