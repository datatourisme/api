# _geo_distance

**_geo_distance** réalise une requête géographique sur la base de coordonnées et de distance. 
La requête renverra donc les POI situés autours d'un point central donné en paramètres.

```graphql
{
    poi(
        filters: [
            { <propriété> { _geo_distance: {lng: <x> , lat: <y> , distance: <rayon> } }}}
        ]
    )
    {
        total
    }
}
```

### Exemples ###

```graphql
{
    poi(
        filters: [
            { isLocatedAt: {schema_geo: { _geo_distance: {lng: "2.57" , lat: "44.34" , distance: "1" } }}}
        ]
    )
    {
        total
    }
}
```
Cette requête extraira le nombre total de **POI** qui se situent à moins d'un mètre des coordonnées de longitude 2.57 et latitude 44.34 . Le résultat sera de ce type :
```json
{
  "data": {
    "poi": {
      "total": 1
    }
  }
}
```