# _in

Détermine une condition d'égalité sur un tableau. L'opérateur **_in** extrait les POI dont la propriété visée est égale à l'une des valeurs fournies.

```graphql
{
    poi(
        filters: [
            { <propriété>: {_in: [<valeur1>, <valeur2>, ...]} }
        ]
    )
    {
        results {
            <propriété_extraite>
        }
    }
}
```
### Comportement ###

- Egalité valeur de type scalaire :
l'égalité peut porter sur une valeur de type entier, réel, chaîne, booléen...

- Egalité valeur de type ressource :
l'égalité peut porter sur un **POI**, une personne, une adresse...

### Exemples ###

```graphql
{
  poi(
    filters: [
      {
        allowedPersons: {
          _in: [11,30]
        }
      }
    ]
  )
  {
    results{
      dc_identifier
    }
  }
}
```
Cette requête extraira tous les identifiants des **POI** où le nombre de personnes autorisées (**allowedPersons**) est égal à 11 ou 30. Le résultat sera de ce type :
```json
{
  "data": {
    "poi": {
      "results": [
        {
          "dc_identifier": [
            "TFO308495934640"
          ]
        }
      ]
    }
  }
}
```