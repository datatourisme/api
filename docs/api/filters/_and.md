# _and

**_and** réalise une opération logique **AND** sur un tableau d'expressions.

```graphql
{
    poi(
        filters: [
            { _and : [
                    { <expression1> },
                    { <expression2> }
                ]
            }
        ]
    )
    {
        results {
            <propriété_extraite>
        }
    }
}
```
### Notation alternative
Une notation équivalente à la précédente est :
```graphql
{
    poi(
        filters: [
            { <expression1> },
            { <expression2> }
        ]
    )
    {
        results {
            <propriété_extraite>
        }
    }
}
```



### Exemples ###

```graphql
{
    poi(
        filters: [
            { _and : [
                    {hasDescription: {shortDescription: {_text: "trop"}}},
                    {rdf_type: {_eq: "http://test"}}
                ]
            }
        ]
    )
    {
        total,
        results {
            dc_identifier
        }
    }
}
```
Cette requête extraira tous les identifiants des **POI** dont la ressource est de type "http://test" et dont la description contient **"trop"**. Le résultat sera de ce type :
```json
{
  "data": {
    "poi": {
      "results": [
        {
          "dc_identifier": [
            "TFO308495934640"
          ]
        }
      ]
    }
  }
}
```