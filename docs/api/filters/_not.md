# _not

**_not** réalise une opération logique **NOT** sur un tableau d'expressions. 

```graphql
{
    poi(
        filters: [
            { 
                _not : [
                    { <expression1> },
                    { <expression2> }
                
                ]
            }
        ]
    )
    {
        results {
            <propriété_extraite>
        }
    }
}
```


### Exemples ###

```graphql
{
    poi(
        filters: [
            { _not : [
                    {allowedPersons: {_lt: "30"}},
                    {allowedPersons: {_gt: "30"}}
                ]
            },
            {allowedPersons: {_gt: "0"}}
        ]
    )
    {
        results {
            dc_identifier
        }
    }
}
```
Cette requête extraira tous les identifiants des **POI** qui n'autorisent pas moins de 30 personnes ni plus de 30 personnes. Le résultat sera de ce type :
```json
{
  "data": {
    "poi": {
      "results": [
        {
          "dc_identifier": [
            "TFO308495934640"
          ]
        }
      ]
    }
  }
}
```