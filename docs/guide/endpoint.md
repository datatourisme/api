# Point d'accès GraphQL

Afin de permettre à des applicatifs tiers de tirer partie de l'API GraphQL (au travers du client
[Apollo](https://www.apollographql.com/client) par exemple), vous devez mettre en place un point d'accès HTTP. Voici
un exemple de code PHP permettant de le faire :

```php
<?php

header('Access-Control-Allow-Credentials: true', true);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type');

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    return;
}

if (isset($_SERVER['CONTENT_TYPE']) && $_SERVER[
'CONTENT_TYPE'] === 'application/json') {
    $rawBody = file_get_contents('php://input');
    $requestData = json_decode($rawBody ?: '', true);
} else {
    $requestData = $_POST;
}
$payload = isset($requestData['query']) ? $requestData['query'] : null;

require __DIR__ . '/vendor/autoload.php';

$processor = \Datatourisme\Api\DatatourismeApi::create('http://localhost:9999/blazegraph/namespace/kb/sparql');
$response = $processor->process($payload);
header('Content-Type: application/json');
echo json_encode($response);
exit;

```