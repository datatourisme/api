<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\SparqlResolver\Filter\Conditional;

use Datatourisme\Api\Resolver\SparqlResolver\Filter\ConditionalFilterInterface;
use Datatourisme\Api\Resolver\SparqlResolver\Sparql\Functions\Filter;
use Datatourisme\Api\Resolver\SparqlResolver\Utils\SparqlUtils;
use Datatourisme\Api\Schema\Field\AbstractField;

class In implements ConditionalFilterInterface
{
    public function getName()
    {
        return '_in';
    }

    public function generate($subject, AbstractField $fieldDef, $value)
    {
        $value = array_map(function ($v) use ($fieldDef) {
            return SparqlUtils::formatFieldValue($v, $fieldDef);
        }, $value);

        $filter = new \Datatourisme\Api\Resolver\SparqlResolver\Sparql\Functions\In($subject, $value);

        return new Filter($filter);
    }
}
