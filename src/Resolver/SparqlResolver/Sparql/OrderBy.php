<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\SparqlResolver\Sparql;

class OrderBy
{
    private $_orderBy;

    public function __construct($orderBy)
    {
        $this->_orderBy = $orderBy;
    }

    public function __toString()
    {
        return 'ORDER BY '.$this->_orderBy;
    }
}
