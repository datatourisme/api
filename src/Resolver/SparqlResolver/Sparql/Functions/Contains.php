<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\SparqlResolver\Sparql\Functions;

class Contains
{
    private $_contains;

    public function __construct($contains)
    {
        $this->_contains = $contains;
    }

    public function __toString()
    {
        return sprintf('CONTAINS(%s)', $this->_contains);
    }
}
