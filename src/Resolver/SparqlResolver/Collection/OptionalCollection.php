<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Resolver\SparqlResolver\Collection;

class OptionalCollection extends AbstractCollection
{
    /**
     * @return string
     */
    public function __toString()
    {
        if (!$this->count()) {
            return '';
        }

        return "{}\nUNION {".implode("}\nUNION {", $this->getArrayCopy()).'}';
    }
}
