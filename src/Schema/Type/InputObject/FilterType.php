<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Schema\Type\InputObject;

use Youshido\GraphQL\Type\InputObject\AbstractInputObjectType;
use Youshido\GraphQL\Type\ListType\ListType;
use Youshido\GraphQL\Type\Scalar\StringType;

class FilterType extends AbstractInputObjectType
{
    public function build($config)
    {
        $stringFilters = ['_eq', '_gt', '_gte', '_lt', '_lte', '_ne', '_text'];
        foreach ($stringFilters as $filter) {
            $config
                ->addField($filter, [
                    'type' => new StringType(),
                    'resolve' => function ($value) {
                        return $value;
                    },
                ]);
        }
        $arrayFilters = ['_in', '_nin'];
        foreach ($arrayFilters as $filter) {
            $config
                ->addField($filter, [
                    'type' => new ListType(new StringType()),
                    'resolve' => function ($value) {
                        return $value;
                    },
                ]);
        }

        $logicalFilters = ['_and', '_or', '_not'];
        foreach ($logicalFilters as $filter) {
            $config
                ->addField($filter, [
                    'type' => new ListType(new self()),
                    'resolve' => function ($value) {
                        return $value;
                    },
                ]);
        }
    }

    public function getDescription()
    {
        return 'Paramètre permettant de filtrer les résultats';
    }
}
