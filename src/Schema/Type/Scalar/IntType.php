<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Schema\Type\Scalar;

class IntType extends \Youshido\GraphQL\Type\Scalar\IntType
{
    public function isValidValue($value)
    {
        return is_null($value) || is_numeric($value);
    }
}
