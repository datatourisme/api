<?php
/**
 * Created by PhpStorm.
 * User: blaise
 * Date: 25/05/18
 * Time: 11:53.
 */

namespace Datatourisme\Api\Schema\Type\Object;

use Youshido\GraphQL\Config\Object\ObjectTypeConfig;
use Youshido\GraphQL\Type\Object\AbstractObjectType;
use Youshido\GraphQL\Type\Scalar\StringType;

class LangStringType extends AbstractObjectType
{
    /**
     * @param ObjectTypeConfig $config
     */
    public function build($config)
    {
        // you can define fields in a single addFields call instead of chaining multiple addField()
        $config->addFields([
            'value' => [
                'type' => new StringType(),
                'description' => 'Literal value',
            ],
            'lang' => [
                'type' => new StringType(),
                'description' => 'Literal language',
            ],
        ]);
    }

    /**
     * For legacy.
     *
     * @param $value
     *
     * @return bool
     */
    public function isValidValue($value)
    {
        return is_array($value) || is_string($value);
    }
}
