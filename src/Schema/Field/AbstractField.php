<?php
/**
 * This file is part of the DATAtourisme project.
 *
 *  @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Datatourisme\Api\Schema\Field;

abstract class AbstractField extends \Youshido\GraphQL\Field\AbstractField
{
    /**
     * @return mixed
     */
    abstract public function getUri();

    /**
     * @return mixed
     */
    public function getRangeUri()
    {
        return null;
    }

    /**
     * @return bool
     */
    public function isDatatypeProperty()
    {
        return false !== strpos($this->getRangeUri(), 'http://www.w3.org/2001/XMLSchema#');
    }

    /**
     * @return bool
     */
    public function getDatatype()
    {
        return $this->isDatatypeProperty() ? $this->getRangeUri() : null;
    }
}
